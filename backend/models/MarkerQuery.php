<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Marker]].
 *
 * @see Marker
 */
class MarkerQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Marker[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Marker|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
