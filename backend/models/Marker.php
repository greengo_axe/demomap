<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "marker".
 *
 * @property string $name
 * @property float|null $lat
 * @property float|null $lng
 */
class Marker extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'marker';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['lat', 'lng'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'lat' => Yii::t('app', 'Lat'),
            'lng' => Yii::t('app', 'Lng'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return MarkerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MarkerQuery(get_called_class());
    }
}
