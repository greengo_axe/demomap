<?php

/* @var $this yii\web\View */

$this->title = 'Demo - Back-end';
use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use \yii\web\JsExpression;
use yii\web\View;
?>
<div class="site-index">
    <div class="jumbotron">
        <h1>Demo Map</h1>

        <p class="lead"></p>
    </div>
    <div class="body-content">
        <div class="row">
            <div class="col-lg-6">
                <h3>Markers</h3>
                <p>
                    <?= Html::a("Edit", Url::toRoute(['marker/index'], true),['class'=>'btn btn-default index-a']) ?> 
                </p>
            </div>
            <div class="col-lg-6">
                <h3>Users</h3>
                <p>
                    <?= Html::a("Edit", Url::toRoute(['user/index'], true),['class'=>'btn btn-default index-a']) ?> 
                </p>
            </div>
        </div>
    </div>
</div>