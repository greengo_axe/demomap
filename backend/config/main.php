<?php
use cinghie\mailchimp\components\Mailchimp as MailchimpComponent;
use cinghie\mailchimp\Mailchimp;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'mailchimp' => [
        'class' => Mailchimp::class,
        'showFirstname' => true,
        'showLastname' => true
    ]
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'csrfCookie' => [
                'httpOnly' => true,
                'path' => '/admin'
             ],
        ],
        'mailchimp' => [
		'class' => MailchimpComponent::class,
		'apiKey' => '6d01d80ba5620ed7fe203efcadaf7f0a-us17'
	],

        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
            'identityCookie' => [
		'name' => '_identity-backend', 
		'path' =>'/admin',
		'httpOnly' => true,
                'domain'=>'var-demo.map.dev',
                'secure'=>true
	    ],
            'authTimeout' => (60*15) // 15 minutes
        ],

        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
    	    'cookieParams' => [
            	'path' => '/admin',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];
