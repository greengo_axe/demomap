<?php
return [
    'adminEmail' => 'admin@demomap.dev',
    'supportEmail' => 'support@demomap.dev',
    'senderEmail' => 'noreply@demomap.dev',
    'senderName' => 'var-demomap mailer',
    'user.passwordResetTokenExpire' => 3600,
];
