<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\HtmlPurifier;


class Common extends \yii\db\ActiveRecord {
    private $commonStringFields;
        
    public function __construct($config = array()) {
        parent::__construct($config);
        
        $this->commonStringFields = array('text','name','date','email');
        
    }
    
    
       /**
     * a universal query with one condition
     * @param int $user_id
     * @return array $query_res
     */
    public function findOneWhere($table,$where_field,$where_value,$sndCond=false,$like=false)
    {
        $query = (new \yii\db\Query());
        $query_res = $query->select('*')
        ->from($table);
        if(!$like){
            $query->where($where_field.'=:'.$where_field,[':'.$where_field=>$where_value]);
        }
        else{
            $query->where($where_field.' LIKE :'.$where_field,[':'.$where_field=>''.$where_value.'']);
        }
        
        if($sndCond){
            $query->andWhere($sndCond);
        }
        
        $query_res = $query->one();
        return $query_res;
    }
    
        
    /**
     * updates rows with condition and return number of rows updated
     * @param string $table
     * @param string $field
     * @param string $value
     * @return int
     */
    public function updateR($table, $field, $value,$condition) {
        return $this->updateRows($table, $field, $value,$condition);
    }
    
    /**
     * updates rows with condition and return number of rows updated
     * @param string $table
     * @param string $field
     * @param string $value
     * @return int
     */
    private function updateRows($table,$field,$value,$condition){
        $num_rows = Yii::$app->db->createCommand()->update($table, [$field => $value], $condition)->execute();
        return $num_rows;
    }
    
        /**
     * Multiple rows saved at time
     * @param string $table
     * @param array $fields
     * @param array $values
     * @return int number of rows created
     */
    public function multipleSave($table,$fields,$values){
        return $this->multipleInsert($table,$fields,$values);
    }
    
    /**
     * Multiple rows saved at time
     * @param string $table
     * @param array $fields
     * @param array $values
     * @return int number of rows created
     */
    private function multipleInsert($table,$fields,$values){
        $num_rows = Yii::$app->db->createCommand()->batchInsert($table, $fields, $values)->execute();
        return $num_rows;
    }
    
        /**
     * gets all items from a table -- format index page
     * @param type $table
     * @param type $cache
     * @return type
     */
     public function getAllItems($table,$cache=false)
    {
        $query = (new \yii\db\Query());
        $query_res = $query->select('*')
        ->from($table);
                
        if($cache){
            $query->cache(3600);
        }
        
        $query->orderBy('id ASC');
        
        $query_res = $query->all();
        return $query_res;
    }
    
        public function htmlDecodeArray($data,$sanitize = true){
        $entities = array(); 
        $htmlpurifier = new HtmlPurifier();
        
        $fields = $this->commonStringFields;
        foreach ($data as $key =>$v){
            if(in_array($key, $fields)){// strings need double quotes
                $v = html_entity_decode(htmlspecialchars_decode($v));
                if($sanitize){
                    $v = $htmlpurifier->process($v);
                }
                $entities[$key]=$v;
            }
            else{
                $entities[$key]= $v;
            }
        }
        return $entities;
    }
    
}
