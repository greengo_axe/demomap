
function mapInit(){
            mymap = L.map('mapbox').setView([42.0, 12.0], 2);//lat,lng

            var Stamen_TonerLite = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner-lite/{z}/{x}/{y}.{ext}', {
                attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                subdomains: 'abcd',
                minZoom: 2,
                maxZoom: 15,
                ext: 'png'
            }).addTo(mymap);
            
            return mymap;
         }

function showMap(mymap,data){
    var marker = [];
    
    LeafIcon = L.Icon.extend({
	options: {
            iconSize:     [32, 37],
            iconAnchor:   [10,10],
	}
    });
    
    for(i=0;i<=data.length;i++){
        if(!isUndefined(data[i])){
            marker[i] = L.marker([data[i].lat, data[i].lng]).addTo(mymap);
            marker[i].bindPopup("Marker: "+data[i].name).openPopup();
        }
    }
    
}
