<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use \yii\web\JsExpression;
use yii\web\View;
use \yii\helpers\ArrayHelper;
use \yii\helpers\Json;
use common\models\Common;
use yii\helpers\HtmlPurifier;
use yii\jui\Tabs;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

$arrayHelper = new ArrayHelper();
$jsonHelper = new Json();
$modelCommon = new Common();
$htmlpurifier = new HtmlPurifier();
$markers = array();
    
$this->registerJs(
        "  var mymap;"
        . "var markers;"
        . "var dataFilterName;",
        View::POS_HEAD
);

$this->registerJs(
        "
            mymap = mapInit();
        jQuery.ajax({
            'dataType':'json',
            type     :'GET',
            cache    : false,
            url  : '/index.php?r=site/ajaxindex',
        }).done(function(response) {
                showMap(mymap,response);            
                
            });
    
",
        View::POS_LOAD
);
?>

<?php $this->title = 'Demo Map'; ?>
<div class="" id="rowScrollerAndMap">
    
    <?php 
    /**
     * Map container
     */
    ?>
    <div class="" id="mapContainer">
        <div class="" id="mapbox-col" >
            <center><div id="mapbox" class="" style="height: 768px;width: 1024px;"></center>
            </div>
        </div>
    </div>
</div>

