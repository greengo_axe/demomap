<?php
namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\Marker;
use frontend\models\MarkerSearch;
use frontend\models\Event;
use frontend\models\EventSearch;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\Markerhassocialnetwork;
use frontend\models\MarkerhassocialnetworkSearch;
use frontend\models\Communitystat;
use yii\helpers\Url;
use yii\base\Response;
use yii\filters\HostControl;
use common\models\Common;
/**
 * Site controller
 */
class SiteController extends Controller
{
    
    protected $allowedHosts; 
    
    public function __construct($id, $module, $config = array()) {
        parent::__construct($id, $module, $config);
        
        
    }
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['ajaxindex','index'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout,ajaxindex','index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    
    public function actionIndex()
    {
        return $this->render('index', [
        ]);
    }

    /**
     * Retrieves all markers approved via ajax
     * @return json $markers
     */
    public function actionAjaxindex()
    {
        $modelCommon = new \common\models\Common();
        $dataProvider;
        $markers = array();
        
        if(Yii::$app->request->isAjax){
            $dataProvider = $modelCommon->getAllItems('marker',false);
            
            foreach($dataProvider as $k=>$row){
                $markers[] = $modelCommon->htmlDecodeArray($row);
            }
            
            return $this->asJson($markers);
        }
    }
    
    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {

        return $this->redirect(Url::base('').'/admin/index.php?r=site/login');
        die();
        }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

   

    /**
     * Displays about page.
     *
     * @return mixed
     */
//    public function actionAbout()
//    {
//        return $this->render('about');
//    }
 
    
    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        return $this->redirect(Url::base('').'/admin/index.php?r=site/signup');
        die();
    }
}