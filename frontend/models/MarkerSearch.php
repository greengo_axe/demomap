<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Marker;
use common\models\Common;

/**
 * MarkerSearch represents the model behind the search form of `app\models\Marker`.
 */
class MarkerSearch extends Marker
{

    /**
     * {@inheritdoc}
     */
    
    public function __construct($config = array()) {
        parent::__construct($config);
        $modelCommon = new \common\models\Common();
    }
    
    public function rules()
    {
        return [
                [['id'], 'integer'],
                [['name'], 'string', 'max' => 255],
                [['lat'], 'number','min'=>-90,'max'=>90],
                [['lng'], 'number','min'=>-180,'max'=>180],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
}
