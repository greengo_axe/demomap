<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "marker".
 *
 * @property int $id
 * @property string $name
 * @property double $lat
 * @property double $lng
 
 */
class Marker extends \yii\db\ActiveRecord
{
    private $currentTeam;
    
    public function __construct($config = array()) {
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'marker';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        
        
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['lat'], 'number','min'=>-90,'max'=>90],
            [['lng'], 'number','min'=>-180,'max'=>180],
            [['lat'], 'default', 'value' => 0],
            [['lng'], 'default', 'value' => 0],
        ];
        
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'lat' => Yii::t('app', 'Latitude'),
            'lng' => Yii::t('app', 'Longitude'),
        ];
    }

    
    /**
     * {@inheritdoc}
     * @return MarkerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MarkerQuery(get_called_class());
    }
}
