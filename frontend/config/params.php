<?php
return [
    'adminEmail' => 'admin@demomap.dev',
    'supportEmail' => 'noreply@demomap.dev',
    'senderEmail' => 'noreply@demomap.dev',
    'senderName' => 'Demo map',
    'user.passwordResetTokenExpire' => 3600,
];
