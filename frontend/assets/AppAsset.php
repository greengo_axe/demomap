<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main front-end application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'css/site.css',
        'css/site.css',
        'css/jqueryui/jquery-ui.min.css',
        'css/jqueryui/jquery-ui.structure.min.css',
        'css/jqueryui/jquery-ui.theme.min.css',
        'css/leaflet/leaflet.css',
    ];
    public $js = [
        'js/leaflet/leaflet.js',
        'js/leaflet/leaflet-providers.js',
        'js/site/map_home.js',
        'js/site/common.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapAsset',
        
    ];
}
